console.log("Hello Batch 242!");

// JavaScript Operators

// Arithematic Operators

let x = 1397;
let y = 7831;

// Sum
let sum = x + y;
console.log("Result of addition operator: " + sum);

// Difference
let difference = x - y;
console.log("Result of subtraction operator: " + difference);

// Product
let product = x * y;
console.log("Result of multiplication operator: " + product);

// Difference
let quotient = x / y;
console.log("Result of division operator: " + quotient);

// Modulus
let remainder = x % y;
console.log("Result of modulo operator: " + remainder);

// Assignment operators
// Basic assignment operators (=)
let assignmentNumber = 8;

// Addition assignment operator (+=)
assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber);

assignmentNumber -= 2;
console.log("Result of subtraction assignment operator: " + assignmentNumber);

assignmentNumber *= 2;
console.log("Result of multiplication assignment operator: " + assignmentNumber);

assignmentNumber /= 2;
console.log("Result of division assignment operator: " + assignmentNumber);

assignmentNumber %= 2;
console.log("Result of modulo assignment operator: " + assignmentNumber);

// Multiple Operators and Parenthesis
let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mdas operation: " + mdas);

// The order of operations can be changed by adding parenthesis to the logic
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation: " + pemdas);

// Increment and Decrement
let z = 1;

// Pre-increment
let increment = ++z;
console.log("Result of pre-increment operation: " + increment);
console.log("Result of pre-increment operation: " + z);

// Post-increment
increment = z++;
console.log("Result of post-increment operation: " + increment);
console.log("Result of post-increment operation: " + z);

// Pre-decrement
let decrement = --z;
console.log("Result of pre-decrement operation: " + decrement);
console.log("Result of pre-decrement operation: " + z);

// Post-decrement
decrement = z--;
console.log("Result of post-decrement operation: " + decrement);
console.log("Result of post-decrement operation: " + z);

// Type Coercion - the automatic or implicit conversion of values from one data type to another.
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion);
console.log(typeof coercion);

let numE = true + 1;
console.log(numE);
console.log(typeof numE);

// Comparision Operators (==)
let juan = 'juan';

// Equality operator (==)
console.log("Equality operator");
console.log(1 == 1);
console.log(1 == '1');
console.log(0 == false);
console.log('juan' == 'juan');
console.log('juan' == juan);
console.log('\n');

// Inequality operator (!=)
console.log("Inequality operator");
console.log(1 != 1);
console.log(1 != '1');
console.log(0 != false);
console.log('juan' != 'juan');
console.log('juan' != juan);
console.log('\n');

// Strict Equality operator
console.log("Strict Equality operator");
console.log(1 === 1);
console.log(1 === '1');
console.log(0 === false);
console.log('juan' === 'juan');
console.log('juan' === juan);
console.log('\n');


// Strict Inequality operator
console.log("Strict Inequality operator");
console.log(1 !== 1);
console.log(1 !== '1');
console.log(0 !== false);
console.log('juan' !== 'juan');
console.log('juan' !== juan);
console.log('\n');

// Relational operators
let a = 50;
let b = 65;
let isGreaterThan = a > b;
let isLessThan = a < b;
let isGTorEqual = a >= b;
let isLTorEqual = a <= b;;

console.log("Relation operators");
console.log(isGreaterThan);
console.log(isLessThan);
console.log(isGTorEqual);
console.log(isLTorEqual);
console.log('\n');

let numStr = "30";
console.log("Relational operator with type coercion");
console.log(a > numStr);
console.log('\n');

// Logical operators
let isLegalAge  = true;
let isRegistered = false;

// And Operator (&&)
// Returns true if all operands are true
let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of logical And operator: " + allRequirementsMet);

// Or Operator (&&)
let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of logical Or operator: " + someRequirementsMet);

// Not Operator (!)
let someRequirementsNotMet = !isRegistered;
console.log("Result of logical Not operator: " + someRequirementsNotMet);
